this module provides a field formatter for commerce_product_reference fields
that is a link to the same node in a different display mode. this allows seperating the add to cart form out onto a seperate page.

a new display mode is provided for nodes (only on those with commerce_reference_fields) that can be used as the view mode being linked to (the product selector)

to override this behaviour on a per entity basis use the view_mode_field module

possible enhancements: show cart in ctools modal/colorbox overlay

TODO:

- selectable link text
- selectable path
